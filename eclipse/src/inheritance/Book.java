/*Sirine Aoudj, 1935903*/
package inheritance;

public class Book {
	
	protected String title;
	private String author;
	
	public Book(String title, String author){
        this.title = title; 
        this.author = author;
	}
	
	public String getTitle() {
		return this.title;
	}
	public String getAuthor() {
		return this.author;
	}
	
	public String toString() {
        final StringBuilder s = new StringBuilder();
        s.append("\n");
        s.append("Book Title: "+ this.title );
        s.append("\n");
        s.append("Author: "+ this.author);
        s.append("\n");
        return s.toString();
     }
	
}