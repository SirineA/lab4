/*Sirine Aoudj, 1935903*/
package inheritance;

public class BookStore {
	
public static void main(String[] args) {
		
		ElectronicBook book = new ElectronicBook("john","potato",50);
		System.out.println(book);
		
		Book[] books = new Book[5];
		
		books[0] = new Book("title1","essafi");
		books[1] = new ElectronicBook("title2","audet",40);
		books[2] = new Book("title3","aoudj");
		books[3] = new ElectronicBook("title4","hammache",30);
		books[4] = new ElectronicBook("title5","tremblay",45);
		
		for(int i =0; i< books.length; i++) {
			System.out.println(books[i]);
		}
	}

}
