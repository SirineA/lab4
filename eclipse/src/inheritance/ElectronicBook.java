/*Sirine Aoudj, 1935903*/
package inheritance;

public class ElectronicBook extends Book {

	int numberBytes;
	
	public ElectronicBook (String title, String author, int numberBytes) {
		super(title, author);
		this.numberBytes = numberBytes;
	}
	
	public String toString() {
        final StringBuilder s = new StringBuilder();
        String fromBase = super.toString();
        s.append(fromBase);
        s.append("Number Of Bytes: "+ this.numberBytes);
        s.append("\n");
        return s.toString();
     }

}
