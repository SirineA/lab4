/*Sirine Aoudj, 1935903*/
package geometry;

public interface Shape {
	 
	double getArea();
	double getPerimeter();

}
