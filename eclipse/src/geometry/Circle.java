/*Sirine Aoudj, 1935903*/
package geometry;

public class Circle implements Shape {

	private double radius;
	
	public Circle(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	public double getArea() {
		double area;
		area = Math.PI*Math.pow(this.radius, 2);
		return area;
	}
	
	public double getPerimeter() {
		double perimeter;
		perimeter = Math.PI*this.radius*2;
		return perimeter;
	}
	
}
