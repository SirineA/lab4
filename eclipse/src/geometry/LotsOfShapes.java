/*Sirine Aoudj, 1935903*/
package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
	
		Shape[] shapes = new Shape[5];
		
		shapes[0] = new Circle(3);
		shapes[1] = new Circle(4.5);
		shapes[2] = new Rectangle(5,8);
		shapes[3] = new Rectangle(12.5,7);
		shapes[4] = new Square(6);

		for(int i =0; i< shapes.length; i++) {
			
			double area = shapes[i].getArea();
			double perimeter = shapes[i].getPerimeter();
			System.out.println(area + ", "+perimeter);
		}
	}

}
